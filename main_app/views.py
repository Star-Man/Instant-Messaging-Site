# -*- coding: utf-8 -*-
from django.http import HttpResponse
from django.http import HttpRequest
from channels import Group
from django.shortcuts import render
from django.views.generic import TemplateView
from login_handling.models import Profile, sent_messages, recieved_messages
from django.contrib.auth.models import User
import datetime
import time
from django.contrib.auth.decorators import user_passes_test
import datetime
from django.db.models import Q
import json
from .models import Document
from .forms import DocumentForm

from django.template import RequestContext
from django.http import HttpResponseRedirect
from django.urls import reverse

def main_base(request):
    current_user = request.user
    users = User.objects.all()


    messages_raw = list(sent_messages.objects.filter(Q(sender=current_user) | Q(reciever=current_user)).values('message','reciever','sender'))
    messages = json.dumps(messages_raw)
    print(messages)
    if current_user is None:
        theme_color = "Dark"
        accent_color = "Orange"
    else:
        theme_color = Profile.objects.filter(user = current_user).values_list('theme_color', flat=True)[0]
        accent_color = Profile.objects.filter(user = current_user).values_list('accent_color', flat=True)[0]
    print (accent_color)
    return render(
        request,
        'main.html',
        context={'theme_color':theme_color, 'accent_color':accent_color, 'users':users, 'messages':messages},
    )

def user_settings(request):
    current_user = request.user
    if current_user is None:
        theme_color = "Dark"
        accent_color = "Orange"
    else:
        theme_color = Profile.objects.filter(user = current_user).values_list('theme_color', flat=True)[0]
        accent_color = Profile.objects.filter(user = current_user).values_list('accent_color', flat=True)[0]
    return render(
        request,
        'settings.html',
        context={'theme_color':theme_color, 'accent_color':accent_color},
    )


def change_theme(request):
    if request.user.is_authenticated:
        theme_color = request.GET.get('theme_color')
        accent_color = request.GET.get('accent_color')
        print (accent_color)
        change_theme =Profile.objects.update(theme_color=theme_color, accent_color=accent_color)
        print("Theme changed")


    return HttpResponse(request)


def save_sent_message(request):
    if request.user.is_authenticated:

        reciever = request.GET.get('reciever')
        message = request.GET.get('message')

        sent_message =sent_messages(sender= request.user, message=message, reciever=reciever)
        sent_message.save()
        print ("sent")
    return HttpResponse(request)

def upload(request):
 # Handle file upload
    if request.method == 'POST':
        form = DocumentForm(request.POST, request.FILES)
        if form.is_valid():
            newdoc = Document(docfile=request.FILES['docfile'])
            newdoc.save()

            # Redirect to the document list after POST
            return HttpResponseRedirect(reverse('upload'))
    else:
        form = DocumentForm()  # A empty, unbound form

    # Load documents for the list page
    documents = Document.objects.all()

    # Render list page with the documents and the form
    return render(
        request,
        'list.html',
        {'documents': documents, 'form': form}
)
